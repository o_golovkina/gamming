﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GammModule
{
    class Program
    {
        static void CodingAlphabet(Dictionary<char, (int, string)> alphabet, int gamm)
        {
            var keys = alphabet.Keys;
            foreach (var k in keys.ToArray())
                alphabet[k] = (alphabet[k].Item1, Convert.ToString(alphabet[k].Item1, gamm));
        }

        static Dictionary<int, char> AlphabetCodeSymbol(Dictionary<char, (int, string)> alphabet, int gamm)
        {
            Dictionary<int, char> alphDict = new Dictionary<int, char>();
            var keys = alphabet.Keys;
            foreach (var k in keys.ToArray())
                alphDict.Add(alphabet[k].Item1, k);

            return alphDict;
        }

        static string CorrectKey(string key, string message)
        {
            int count = message.Length - key.Length;

            if (count > 0)
            {
                for (int i = 0; i < count; i++)
                    key += key[i];
            }
            else if (count < 0)
            {
                key = key.Remove(message.Length, Math.Abs(count));
            }

            return key;
        }

        static string CorrectKey(string key, int messageLength)
        {
            int count = messageLength - key.Length;

            if (count > 0)
            {
                for (int i = 0; i < count; i++)
                    key += key[i];
            }
            else if (count < 0)
            {
                key = key.Remove(messageLength, Math.Abs(count));
            }

            return key;
        }

        static List<string> EncryptMessage(string message, string key, int gamm)
        {
            List<string> encryptMessage = new List<string>();

            for (int i = 0; i < message.Length; i++)
            {
                int x = 0, y = 0;
                x = GetCode(message, i, x);
                y = GetCode(key, i, y);
                int z = x ^ y;
                encryptMessage.Add(Convert.ToString(z, gamm));
            }
            return encryptMessage;
        }

        static char GetSymbol(int code)
        {
            char x = '#';
            if (RUS_D.ContainsKey(code))
                x = RUS_D[code];
            else if (ENG_D.ContainsKey(code))
                x = ENG_D[code];
            return x;
        }

        static List<char> DecryptMessage(List<string> encryptMessage, string key, int gamm)
        {
            List<char> decryptMessage = new List<char>();

            for (int i = 0; i < encryptMessage.Count; i++)
            {
                int x = 0, y = 0;
                x = Convert.ToInt32(encryptMessage[i], gamm);
                y = GetCode(key, i, y);
                int z = x ^ y;
                decryptMessage.Add(GetSymbol(z));
            }
            return decryptMessage;
        }

        static string StringToCode(string str)
        {
            string res = "( ";

            foreach (var c in str)
                res += GetCodeByGamm(c) + " ";

            res += ")";

            return res;
        }

        private static int GetCode(string message, int i, int x)
        {
            if (Alphabet.RUS.ContainsKey(message[i]))
                x = Alphabet.RUS[message[i]].Item1;
            else if (Alphabet.ENG.ContainsKey(message[i]))
                x = Alphabet.ENG[message[i]].Item1;
            return x;
        }
        private static string GetCodeByGamm(char symbol)
        {
            string x = "##";
            if (Alphabet.RUS.ContainsKey(symbol))
                x = Alphabet.RUS[symbol].Item2;
            else if (Alphabet.ENG.ContainsKey(symbol))
                x = Alphabet.ENG[symbol].Item2;
            return x;
        }
       
        static Dictionary<int, char> RUS_D;
        static Dictionary<int, char> ENG_D;

        static void Main(string[] args)
        {
            Console.Write("Введите гамму: ");
            int g = Int32.Parse(Console.ReadLine());

            try
            {
                CodingAlphabet(Alphabet.RUS, g);
                CodingAlphabet(Alphabet.ENG, g);
                RUS_D = AlphabetCodeSymbol(Alphabet.RUS, g);
                ENG_D = AlphabetCodeSymbol(Alphabet.ENG, g);
            }
            catch (Exception)
            {
                Console.WriteLine("Не удалось с текущей гаммой перевести в соответствующую систему.");
                Console.ReadLine();
                return;
            }

            Console.Write("Введите сообщение: ");
            string message = Console.ReadLine();

            Console.Write("Введите ключ: ");
            string key = Console.ReadLine();
            
            key = CorrectKey(key, message);

            Console.WriteLine($"Сообщение в {g}-ом виде: {message} {StringToCode(message)}");
            Console.WriteLine($"Ключ в {g}-ом виде: {key} {StringToCode(key)}");

            var encryptedMessage = EncryptMessage(message, key, g);

            Console.WriteLine("Зашифрованное сообщение: ");
            
            foreach (var m in encryptedMessage)
                Console.Write($"{m} ");
            
            key = CorrectKey(key, encryptedMessage.Count);

            var decryptedMessage = DecryptMessage(encryptedMessage, key, g);

            Console.WriteLine("\nРасшифрованное сообщение: ");

            foreach (var m in decryptedMessage)
                Console.Write($"{m}");


            Console.ReadLine();
        }
    }
}
